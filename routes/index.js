
var Promise = require('promise');
var requestHTTPc = null;
var approvedNameValue = 'approved';
var reviewNameValue =   'under-review';
var progressNameValue =   'in-progress';


//Get page, ansestors
function getRootFromAncestors( id ){
    return new Promise(function (fulfill, reject){
        
        requestHTTPc.get(id + '?expand=ancestors', (err, res, body) => {  
            
            if (err) reject(err);
            var dbody = JSON.parse(body);
            var ancestors = dbody.ancestors;
            var mapdAncestors = [];

            ancestors.map((d)=>{
                if(d.type === 'page'){
                    mapdAncestors.push(getAncestorsProps(d.id));
                }
            });
  
            Promise.all(mapdAncestors)
            .then((root)=>{
                var newrt = null;
                root.map((rt)=>{
                    if(rt != false){
                        newrt = rt;
                    }
                });
                fulfill(newrt);
                return;
            }); 
           
        })
    });
}

function getAncestorsProps(id){
    return new Promise(function (fulfill, reject){
        requestHTTPc.get('/rest/api/content/' + id + '/property/workflows?expand=content.childTypes.all', (err, res, body) => {  
            if (err) reject(err);
            var dbody = JSON.parse(body);

            //exit if no result (404)
            if(dbody.statusCode){
                if(dbody.statusCode == 404){
                    fulfill(false);
                    return;
                }
            }

            //look for root match
            if(isRoot(dbody).isRoot == true ){
                fulfill({
                    newPath: '/rest/api/content/' + id,
                    newBody: dbody 
                });
                return;
            }
            fulfill(false);
 
        });
    });
}


function collateStates( source, test ){
    switch(test){
        case approvedNameValue :
            source.approved = true;
            break;
        case reviewNameValue :
            source.review = true;
            break;
            
        case progressNameValue :
            source.progress = true;
            break;
    }
    return source;
}

//Get page, Children
function getPagesDescendant( triggeredData ){
    return new Promise(function (fulfill, reject){
        requestHTTPc.get(triggeredData.triggerID + '/descendant/page', (err, res, body) => {  
            if (err) reject(err);
            var dbody = JSON.parse(body);
            var decend = dbody.results;

            //build array of requests based onID
            var mapdDecend = [];
            decend.map((d)=>{
                if(d.type == 'page')
                mapdDecend.push(getDescendantWorkflow(d.id));
            });

            //run requests to get workflow
            Promise.all(mapdDecend)
            .then((workflowValues)=>{
                var workflowStat = {
                        approved : null,
                        review   : null,
                        progress : null
                    };

                workflowValues.map((d)=>{
                    workflowStat = collateStates(workflowStat, d);
                });

                //add the root page state
                workflowStat = collateStates(workflowStat, triggeredData.workflowLive);

                //descide what the state should be
                if(workflowStat.progress == true) fulfill('in-progress');
                if(workflowStat.review == true && workflowStat.approved == true) fulfill('under-review');
                if(workflowStat.approved == true) fulfill('approved');
                
                reject('error');
                return;
            }); 
        })
    });
}
/**var approvedNameValue = 'Approved';
var reviewNameValue =   'Under Review';
var progressNameValue =   'In progress'; */
function getDescendantWorkflow( id ){
    return new Promise(function (fulfill, reject){
        requestHTTPc.get('/rest/api/content/'+ id +'/property/workflows', (err, res, body) => {  
            if (err) reject(err);
            var dbody = JSON.parse(body);

            if(dbody.statusCode == 404 || typeof dbody.value.name === 'undefined' ){
                //no workflow applied
                //console.log(id + ': no workflow' );
                fulfill(approvedNameValue);
                return;
            } 
            
            if(dbody.key == 'workflows' && dbody.value.name.value){
                //console.log(id + ': ' + dbody.value.name.value );
                var state = dbody.value.name.value;
                fulfill(state.trim().replace(' ', '-').toLowerCase());
                return;
            }

            //console.log(id + ': no match' + dbody.value.name.value );
            fulfill(approvedNameValue);
            
        })
    });
}


/**
 *  updateScrollDocs()
 *  runs 3 steps 
 *  - gets the scroll docs location
 *  - gets the scroll docs properties
 *  - puts the updated body IF an update is required
 * 
 * 
 * 
 */
function updateScrollDocs( pathToScrollDocsRecord, setStateValue ){
    var scrolldocpath = null;
    return new Promise(function (fulfill, reject){
        getScrollDocsID(pathToScrollDocsRecord)        
        .then((pathToScrollDocsProperties)=>{
            scrolldocpath = pathToScrollDocsProperties
            return getScrollDocsProps(pathToScrollDocsProperties);
        })
        .then((propData)=>{
            //are ALL child pages approved
            if(propData.value.workflow.state != setStateValue){
                console.log('valid updating scroll doc to: ' + setStateValue);
                propData.version.number = propData.version.number + 1;
                propData.version.minorEdit = true;
                propData.version.when = new Date().toISOString();
                propData.value.workflow.state = setStateValue;
    
                return putScrollDocsProps(scrolldocpath, JSON.stringify(propData));
            }
            return true; //no update.
        })
        .then((d)=>{
            fulfill(true);
        });
    });
}


//getScrollDocsID()
function getScrollDocsID( path ){
    return new Promise(function (fulfill, reject){
        requestHTTPc.get(path, (err, res, body) => {  
            if (err) reject(err);
            var dbody = JSON.parse(body);
            var id = dbody.results[0].id
            fulfill('/rest/api/content/'+ id +'/property/K15tDocsMetadata');
        })
    });
}

//getScrollDocsProps()
function getScrollDocsProps( path ){
    return new Promise(function (fulfill, reject){
        requestHTTPc.get(path, (err, res, body) => {  
            if (err) reject(err);
            var dbody = JSON.parse(body);
            fulfill({value: dbody.value, version: dbody.version});
        })
    });
}

//putScrollDocsProps()
function putScrollDocsProps( path, bodyData ){
    return new Promise(function (fulfill, reject){
        var dreq = {
            url: path, 
            body: bodyData,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        
        requestHTTPc.put(dreq, (err, res, body) => {         
            if (err) reject(err);
           
            fulfill(body);
        })
    });
}

function isRoot( dbody ){
        var ct = dbody.content.childTypes
        var isRootValue = false;
        var scrollDocPathValue = null;
        //if Scroll-doc appears here its a root
        if(ct['ac:k15t-scroll-document-versions-for-confluence:document']){
            if(ct['ac:k15t-scroll-document-versions-for-confluence:document'].value){
                isRootValue = true;
                scrollDocPathValue = '/child/ac:k15t-scroll-document-versions-for-confluence:document'; // << contains the link to the scroll doc id and data  
            }
        }
        return { isRoot : isRootValue,  scrollDocPath: scrollDocPathValue }
}


function returnRootData( pagePath, dbody ){
    var ct = dbody.content.childTypes
    var scrollDocPathValue = pagePath + isRoot(dbody).scrollDocPath;
    var hasChildPagevalue = false;
    var hasChildPagePath = null;
    var metadataValue = false;

    if(dbody.key == 'workflows' && dbody.value.name.value){
            var stateD = dbody.value.name.value
            metadataValue = stateD.trim().replace(' ', '-').toLowerCase();
    }

    //check if child pages exist
    if(ct['page']){
        if(ct['page'].value){
            hasChildPagevalue = true;
            hasChildPagePath = ct['page']._links.self; 
        }
    }

    return {
        triggerID               : pagePath,                       // target page ID
        rootScrollDocPath       : scrollDocPathValue,       // path to list scroll docs
        scrollDocsID            : null,
        scrollDocsProp          : null, 
        childPages              : hasChildPagevalue,        // boolan - if has decendents
        childPagesPath          : hasChildPagePath,         // path to list decendents
        workflowLive            : metadataValue             // true/false - tells us if the page is approved
    }
}



//Trigger Page details
function rootPageDetails(pagePath ){
    return new Promise(function (fulfill, reject){

        var path =  pagePath +'/property/workflows?expand=content.childTypes.all';
        //console.log('triggerPath: ' + path);

        requestHTTPc.get(path, (err, res, body) => {  
            if (err) reject(err);
            var dbody = JSON.parse(body);
            if(typeof(dbody.content.childTypes) == 'undefined') reject(err);
       
            if(isRoot(dbody).isRoot == true){
                fulfill(returnRootData(pagePath, dbody));
                return;
            }

            //find the root: if its not the root, find it....
            getRootFromAncestors(pagePath).then((dataD)=>{
                fulfill(returnRootData(dataD.newPath, dataD.newBody));
            });

       })
    });
}




export default function routes(app, addon) {
    // Redirect root path to /atlassian-connect.json,
    // which will be served by atlassian-connect-express.
    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });


    //updated content (status change)
    //Fires when workflow is updated
    app.post('/workflow_content_update_scroll', addon.authenticate(), (req, res) => {
        
        if(req.body.title != 'workflows'){
            res.json({ success: 'not a workflow element' });
            return;
        }
        if(!req.body._expandable.container){
            res.json({ error: 'no linked page' });
            return;
        }
        
        //find the liniage, status and scroll doc parent
        requestHTTPc = addon.httpClient(req);

        rootPageDetails(req.body._expandable.container)
        .then((rootData)=>{
            //console.log("rootData: init", rootData);
            //no child pages
            if(rootData.childPages == false){
                return rootData;
            }
         
            //Child pages
            if(rootData.childPages == true){
                return  getPagesDescendant(rootData)
                        .then((d)=>{
                            rootData.workflowLive = d;
                            return rootData;
                        });
            }
        })
        .then((d)=>{
            updateScrollDocs(d.rootScrollDocPath, d.workflowLive);
            res.json(d);
            return;
        })
        
    });
    

    app.post('/workflow_content_create_scroll', addon.authenticate(), (req, res) => {
        console.log("content create");

        if(req.body.title != 'workflows'){
            res.json({ success: 'not a workflow element' });
            return;
        }
        if(!req.body._expandable.container){
            res.json({ error: 'no linked page' });
            return;
        }
        
        requestHTTPc = addon.httpClient(req);

        rootPageDetails(req.body._expandable.container)
        .then((rootData)=>{
            //console.log("rootData: init", rootData);
            //no child pages
            if(rootData.childPages == false){
                return rootData;
            }
         
            //Child pages
            if(rootData.childPages == true){
                return  getPagesDescendant(rootData)
                        .then((d)=>{
                            rootData.workflowLive = d;
                            return rootData;
                        });
            }
        })
        .then((d)=>{
            updateScrollDocs(d.rootScrollDocPath, d.workflowLive);
            res.json(d);
            return;
        })
    });
    
    
    app.post('/workflow_page_create_scroll', addon.authenticate(), (req, res) => {
        console.log("content create");

        if(req.body.title != 'workflows'){
            res.json({ success: 'not a workflow element' });
            return;
        }
        if(!req.body._expandable.container){
            res.json({ error: 'no linked page' });
            return;
        }
        
        requestHTTPc = addon.httpClient(req);

        rootPageDetails(req.body._expandable.container)
        .then((rootData)=>{
            //console.log("rootData: init", rootData);
            //no child pages
            if(rootData.childPages == false){
                return rootData;
            }
         
            //Child pages
            if(rootData.childPages == true){
                return  getPagesDescendant(rootData)
                        .then((d)=>{
                            rootData.workflowLive = d;
                            return rootData;
                        });
            }
        })
        .then((d)=>{
            updateScrollDocs(d.rootScrollDocPath, d.workflowLive);
            res.json(d);
            return;
        })
    });



    //page request
    app.get('/status-update', addon.authenticate(), (req, res) => {
        console.log("status page request");
        res.render('status-update', {
            title: 'Scroll to Docs Status update'
        });
    });

}
